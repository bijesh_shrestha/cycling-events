$(function () {
    $(".image-loader").hide();
    $(document).ajaxStart(function () {
        $(".image-loader").show();
    }).ajaxStop(function () {
        $(".image-loader").hide();
    });

    $('#error-msg').hide();

    $("#login-button").on('click', function() {
        $('#modal-login').addClass("is-active");
    });

    $("#modal-close").click(function() {
        $('#modal-login').removeClass("is-active");
    });

    $('#btn-login').on('click', login);
    $('#btn-logout').on('click', logout);
});

function login() {
    const username = $('#username1').val();
    const password = $('#password1').val();
    $.ajax({
        "url": "login",
        "type": "POST",
        "data" : {
            "username" : username,
            "password" : password
        },
        "dataType": "json",
        "success": function (data) {
            const error = $('#error-msg');
            if (data.success) {
                // redirect to new page
                window.location.href = '../index.jsp';
            } else {
                error.text('Username/Password mistaken.');
                error.show();
            }
        },
        "error": function (a, b, c) {

        }

    });
}

function logout() {
    $.ajax({
        "url": "login",
        "type": "DELETE",
        "success": function () {
            window.location.href = '../userInfo.jsp';
        }
    });
}