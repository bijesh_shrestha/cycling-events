$(function () {
    $("#image-loader").hide();
    $(document).ajaxStart(function () {
        $("#image-loader").show();
    }).ajaxStop(function () {
        $("#image-loader").hide();
    });

    // $('#btnRegister').on('click', registerUser);
    $('form').submit(registerUser);
});

function registerUser(e) {
    const firstname = $('#firstName').val();
    const lastname = $('#lastName').val();
    const email = $('#email').val();
    const phone = $('#phone').val();
    const street = $('#strAddress').val();
    const city = $('#city').val();
    const state = $('#state').val();
    const zip = $('#zipcode').val();
    const club = ""; // need to be implemented
    const username = $('#username').val();
    const password = $('#password').val();
    const confirmPassword = $('#confirmPassword').val();
    $('#pas-err').remove();
    $('#trm-chk-err').remove();
    if (password !== confirmPassword) {
        $('#confirmPassword').after('<p style="color: red" id="pas-err">Password do not match.</p>');
        return false;
    }
    const terms = $('#terms');
    if (!terms.is(':checked')) {
        $('#termsContainer').after('<p style="color: red" id="trm-chk-err">Please read the terms and accept it.</p>');
        return false;
    }
    $.ajax({
        "url": "register",
        "type": "POST",
        "data" : {
            "firstname" : firstname,
            "lastname" : lastname,
            "email" : email,
            "phone" : phone,
            "street" : street,
            "city" : city,
            "state" : state,
            "zip" : zip,
            "club" : club,
            "username" : username,
            "password" : password
        },
        "dataType": "json",
        "success": function (data) {
            const error = $('#error-msg');
            if (data.success) {
                // redirect to new page
                alert('Registration successful. Please login.');
                window.location.href = '../userInfo.jsp';
            } else {
                alert(data.message);
            }
        },
        "error": function (a, b, c) {

        }

    });
    return false;
}