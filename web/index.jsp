<%--
  Created by IntelliJ IDEA.
  User: bijesh, Mrisho , Abiel, Behailu
  Date: 4/22/2018
  Time: 3:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cycling Events</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/login.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <link href="images/bike.png" rel="shortcut icon"/>
    <script defer src="js/jquery.js"></script>
    <script defer src="js/login.js"></script>
    <script src="homePage.js"></script>
    <script src="events.js"></script>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <c:if test="${empty user}">
        <c:redirect url="userInfo.jsp"/>
    </c:if>
</head>
<body>
<%--<div id="loader"></div>--%>
<%--<img src="images/200w.gif" id="loader_img">--%>
<%--http://mumstudents.org/cs472/2014-02/Lectures/ajax/loader.gif--%>
<img src="http://mumstudents.org/cs472/2014-02/Lectures/ajax/loader.gif" id="loader_img">
<section class="hero is-primary has-bg-img">
    <div class="navbar-end">
        <div class="navbar-item">
            <div class="field is-grouped">
                <h2>
                    <p class="control" id="p-welcome">
                        Welcome, ${user.userName} &nbsp;
                    </p>
                </h2>
                <div class="control dropdown is-hoverable is-right" id="p-profile">
                    <div class="dropdown-trigger">
                        <a class="button is-small is-info is-outlined" aria-haspopup="true" aria-controls="dropdown-menu">
                    <span class="icon is-small">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </span>
                            <span>Profile</span>
                        </a>
                    </div>
                    <div class="dropdown-menu" id="dropdown-menu" role="menu">
                        <div class="dropdown-content is-small">
                            <a href="#" class="dropdown-item is-small">
                                View
                            </a>
                            <hr class="dropdown-divider">
                            <a id="btn-logout" class="dropdown-item is-small">
                                Logout
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title is-1">
                Cycling Events
            </h1>
            <h2 class="subtitle">
                Event Manager
            </h2>
        </div>

    </div>
</section>
<%--<div class="container">--%>
<%--<h1 class="title is-1">--%>
<%--Coding Quotes--%>
<%--</h1>--%>
<%--<h2 class="subtitle">--%>
<%--Like your favorites--%>
<%--</h2>--%>
<%--</div>--%>


<section class="section">
    <div class="container">
        <div  class="columns">

            <%--<div class="column">--%>
                <%--<div class="card">--%>
                    <%--<div class="card-content">--%>
                        <%--<h2 class="title">Register New Event</h2>--%>
                        <%--&lt;%&ndash;<h3 class="subtitle">Please Fill the Form Below</h3>&ndash;%&gt;--%>

                        <%--<div class="field">--%>
                            <%--<label class="label">Event Description</label>--%>
                            <%--<div class="control">--%>
                                <%--<textarea class="textarea" id="description" placeholder="Textarea"></textarea>--%>
                            <%--</div>--%>
                        <%--</div>--%>

                        <%--<div class="field">--%>
                            <%--<label class="label">Event Route</label>--%>
                            <%--<div class="control">--%>
                                <%--<div class="select is-fullwidth">--%>
                                    <%--<select id="event_routes">--%>
                                        <%--<option>[Select dropdown]</option>--%>
                                    <%--</select>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                        <%--</div>--%>

                        <%--<div class="field">--%>
                            <%--<label class="label">Event Start Time</label>--%>
                            <%--<div class="control has-icons-left has-icons-right">--%>
                                <%--<input id="expectedStartTime" class="input is-fullwidth"  type="datetime-local"/>--%>

                            <%--</div>--%>
                            <%--&lt;%&ndash;<p class="help is-danger"></p>&ndash;%&gt;--%>
                        <%--</div>--%>
                        <%--<div class="field">--%>
                            <%--<label class="label">Event End Time</label>--%>
                            <%--<div class="control has-icons-left has-icons-right">--%>
                                <%--<input id="expectedEnd" class="input is-fullwidth"  type="datetime-local"/>--%>

                            <%--</div>--%>
                            <%--&lt;%&ndash;<p class="help is-danger"></p>&ndash;%&gt;--%>
                        <%--</div>--%>

                        <%--<div class="field">--%>
                            <%--<div class="control">--%>
                                <%--<label class="checkbox">--%>
                                    <%--<input type="checkbox">--%>
                                    <%--I agree to the <a href="#">terms and conditions</a>--%>
                                <%--</label>--%>
                            <%--</div>--%>
                        <%--</div>--%>

                    <%--</div>--%>
                    <%--<footer class="card-footer">--%>
                        <%--<span class="card-footer-item">--%>
                            <%--<button class="button is-primary" id="submitEvent" >--%>
                                <%--<i class="fa fa-save "></i> Submit</button>--%>
                            <%--&lt;%&ndash;<a href="#" class="button is-primary">&ndash;%&gt;--%>
                                <%--&lt;%&ndash;&ndash;%&gt;--%>
                            <%--&lt;%&ndash;</a>&ndash;%&gt;--%>
                        <%--</span>--%>
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-danger">--%>
                                <%--<i class="fa fa-thumbs-o-down"></i>Cancel--%>
                            <%--</a>--%>
                        <%--</span>--%>

                    <%--</footer>--%>
                <%--</div>--%>
            <%--</div>--%>
            <div class="column">
                <div class="card">
                    <header class="card-header">
                        <span  class="card-footer-item">
                        <button id="available"  class="button is-success is-fullwidth">
                        <%--<i class="fa fa-thumbs-o-up"></i>--%>
                            Subscribe Events
                        </button>
                        </span>
                        <span class="card-footer-item">
                        <button id="register" class="button is-info is-fullwidth">
                        <%--<i class="fa fa-thumbs-o-down"></i>--%>
                            Register Event
                        </button>
                        </span>
                        <span class="card-footer-item">
                        <button id="seeUsers" class="button is-info is-fullwidth">
                        <%--<i class="fa fa"></i>--%>
                            See Users
                        </button>
                        </span>
                    </header><div id="systemUsers" class="card-content">
                    <h2 class="title">A List Of Users</h2>
                    <h3 class="subtitle">Click one To View Details</h3>
                    <table class="table is-striped is-fullwidth">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th><th>Current Location</th>
                        </tr>
                        </thead>
                        <tbody id="listOfUsers">
                        </tbody>
                    </table>
                    </div>
                    <div id="contentEvents" class="card-content">
                        <h2 class="title">Available Events</h2>
                        <h3 class="subtitle">Click one To Subscribe</h3>
                        <table class="table is-striped is-fullwidth">
                            <thead>
                            <tr>
                                <th>Event</th>
                                  <th>Action</th><th>People</th>
                            </tr>
                            </thead>
                            <tbody id="tableAvailable">
                            </tbody>
                        </table></div>
                    <div id="registerForm" class="card-content">
                        <h2 class="title">Register New Event</h2>
                        <%--<h3 class="subtitle">Please Fill the Form Below</h3>--%>

                        <div class="field">
                            <label class="label">Event Description</label>
                            <div class="control">
                                <textarea class="textarea" id="description" placeholder="Textarea"></textarea>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Event Route</label>
                            <div class="control">
                                <div class="select is-fullwidth">
                                    <select id="event_routes">
                                        <option>[Select dropdown]</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Event Start Time</label>
                            <div class="control has-icons-left has-icons-right">
                                <input id="expectedStartTime" class="input is-fullwidth"  type="datetime-local"/>

                            </div>
                            <%--<p class="help is-danger"></p>--%>
                        </div>
                        <div class="field">
                            <label class="label">Event End Time</label>
                            <div class="control has-icons-left has-icons-right">
                                <input id="expectedEnd" class="input is-fullwidth"  type="datetime-local"/>

                            </div>
                            <%--<p class="help is-danger"></p>--%>
                        </div>

                        <div class="field">
                            <div class="control">
                                <label class="checkbox">
                                    <input type="checkbox">
                                    I agree to the <a href="#">terms and conditions</a>
                                </label>
                            </div>
                        </div>

                    </div>
                    <footer class="card-footer">
                    <span class="card-footer-item register-event-footer">
                    <button class="button is-primary" id="submitEvent" >
                    <i class="fa fa-save "></i>Submit</button>
                    <%--<a href="#" class="button is-primary">--%>
                    <%----%>
                    <%--</a>--%>
                    </span>
                    <span class="card-footer-item register-event-footer">
                    <a href="#" class="button is-danger">
                    <i class="fa fa-thumbs-o-down"></i>Cancel
                    </a>
                    </span>

                    </footer>
                </div>
            </div>
            <div class="column">
                <header class="card-header">
                    <span id="emergencyBar" class="card-footer-item">
<button class="button is-danger is-fullwidth">
    Emergency Has Happened
</button>
                    </span>
                </header>
                <div class="card">
                    <div class="card-content">
                        <h2 class="title">Live Events</h2>
                        <h3 id="emergencyMessage"  class="subtitle">
                            Click Event to see Participants
                        </h3>

                        <table class="table is-striped is-fullwidth">
                            <thead>
                            <tr>
                                <th>Event</th>
                                <th>Action</th> <th>People</th>
                            </tr>
                            </thead>
                            <tbody id="liveEvents">
                            </tbody>
                        </table>

                    </div>
                    <footer class="card-footer">
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-success">--%>
                                <%--<i class="fa fa-thumbs-o-up"></i>--%>
                            <%--</a>--%>
                        <%--</span>--%>
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-danger">--%>
                                <%--<i class="fa fa-thumbs-o-down"></i>--%>
                            <%--</a>--%>
                        <%--</span>--%>
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-info">--%>
                                <%--<i class="fa fa-retweet"></i>--%>
                            <%--</a>--%>
                        <%--</span>--%>
                    </footer>
                </div>
            </div>

        </div>
    </div>
</section>

<%--<footer class="footer">--%>
<%--Footer--%>
<%--</footer>--%>
<div class="modal" id="modal-login">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Login</p>
            <button class="delete" aria-label="close" id="modal-close"></button>
        </header>
        <section class="modal-card-body">
            <!-- Content ... -->
            <div class="field">
                <div class="control">
                    <input id="username" class="input is-primary" type="text" placeholder="Username">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <input id="password" class="input is-primary" type="password" placeholder="Password">
                </div>
            </div>
            <div id="image-loader">
                <img src="http://mumstudents.org/cs472/2014-02/Lectures/ajax/loader.gif" alt="loader gif"/>
            </div>
        </section>
        <footer class="modal-card-foot pull-right">
            <button class="button is-success float-right" id="btn-login">Login</button>
            <p class="login-error" id="error-msg">Username/Password mistaken.</p>
        </footer>
    </div>
</div>

</body>
</html>
