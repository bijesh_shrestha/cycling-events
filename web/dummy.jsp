<%--
  Created by IntelliJ IDEA.
  User: bijesh
  Date: 4/24/2018
  Time: 9:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <link rel="stylesheet" href="css/login.css">
    <script defer src="js/jquery.js"></script>
    <script defer src="js/dummy.js"></script>
</head>
<body>

<button class="button is-primary" id="show-modal">Submit</button>

<div class="modal" id="modal-update-event">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Update</p>
            <button class="delete" aria-label="close" id="modal-close"></button>
        </header>
        <section class="modal-card-body">
            <!-- Content ... -->
            <div class="field">
                <div class="control">
                    <input id="location" class="input is-primary" type="text" placeholder="Your current location">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <input id="status" class="input is-primary" type="number" placeholder="Status(number)">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <textarea id="message" class="textarea is-primary" placeholder="Your message..." rows="3"></textarea>
                </div>
            </div>
            <div class="image-loader">
                <img src="http://mumstudents.org/cs472/2014-02/Lectures/ajax/loader.gif" alt="loader gif"/>
            </div>
        </section>
        <footer class="modal-card-foot pull-right">
            <button class="button is-success float-right" id="btn-update">Update</button>
            <p class="login-error" id="error-msg">Error.</p>
        </footer>
    </div>
</div>

</body>
</html>
