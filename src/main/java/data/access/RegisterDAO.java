package data.access;

import models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.HashMap;

public class RegisterDAO {

    public static JSONObject registerUser(User user){
        try {
//            Session session = HibernateUtil.getSessionFactory().openSession();
//            session.beginTransaction();
            EntityManager entityManager = MyEntityManager.getEntityManager();

            entityManager.getTransaction().begin();

            int size = entityManager.createQuery("SELECT u FROM User u WHERE userName = :username")
                    .setParameter("username", user.getUserName())
                    .getResultList()
                    .size();
            if (size>0){
                HashMap<String, Object> map = new HashMap();
                map.put("success", false);
                map.put("message", "Username already taken.");
                return new JSONObject(map);
            }
            entityManager.persist(user);
            entityManager.getTransaction().commit();
            MyEntityManager.closeEntityManagerFactory();
            HashMap<String, Object> map = new HashMap();
            map.put("success", true);
            map.put("message", "Registration successful.");
            return new JSONObject(map);
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            HashMap<String, Object> map = new HashMap();
            map.put("success", false);
            map.put("message", "Some error occurred.");
            return new JSONObject(map);
        }
    }

}
