package models;

import javax.persistence.*;

@Entity
@Table(name = "event_states", schema = "cycling_events_db", catalog = "")
public class EventStates {
    private int id;
    private String description;
    private Byte isEmergency;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "isEmergency", nullable = true)
    public Byte getIsEmergency() {
        return isEmergency;
    }

    public void setIsEmergency(Byte isEmergency) {
        this.isEmergency = isEmergency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventStates that = (EventStates) o;

        if (id != that.id) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (isEmergency != null ? !isEmergency.equals(that.isEmergency) : that.isEmergency != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (isEmergency != null ? isEmergency.hashCode() : 0);
        return result;
    }
}
