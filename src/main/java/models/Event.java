package models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Event {
    private Timestamp actualEndTime;
    private Timestamp actualStartingTime;
    private String eventDescription;
    private Integer statusId;
    private Timestamp extepectedEndTime;
    private Integer ownerId;
    private Integer routeId;
    private Timestamp scheduledTime;
    private int id;

    @Basic
    @Column(name = "actual_end_time", nullable = true)
    public Timestamp getActualEndTime() {
        return actualEndTime;
    }

    public void setActualEndTime(Timestamp actualEndTime) {
        this.actualEndTime = actualEndTime;
    }

    @Basic
    @Column(name = "actual_starting_time", nullable = true)
    public Timestamp getActualStartingTime() {
        return actualStartingTime;
    }

    public void setActualStartingTime(Timestamp actualStartingTime) {
        this.actualStartingTime = actualStartingTime;
    }

    @Basic
    @Column(name = "event_description", nullable = true, length = 255)
    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    @Basic
    @Column(name = "status_id", nullable = true)
    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    @Basic
    @Column(name = "extepected_end_time", nullable = true)
    public Timestamp getExtepectedEndTime() {
        return extepectedEndTime;
    }

    public void setExtepectedEndTime(Timestamp extepectedEndTime) {
        this.extepectedEndTime = extepectedEndTime;
    }

    @Basic
    @Column(name = "owner_id", nullable = true)
    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    @Basic
    @Column(name = "route_id", nullable = true)
    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "scheduled_time", nullable = true)
    public Timestamp getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Timestamp scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (id != event.id) return false;
        if (actualEndTime != null ? !actualEndTime.equals(event.actualEndTime) : event.actualEndTime != null)
            return false;
        if (actualStartingTime != null ? !actualStartingTime.equals(event.actualStartingTime) : event.actualStartingTime != null)
            return false;
        if (eventDescription != null ? !eventDescription.equals(event.eventDescription) : event.eventDescription != null)
            return false;
        if (statusId != null ? !statusId.equals(event.statusId) : event.statusId != null) return false;
        if (extepectedEndTime != null ? !extepectedEndTime.equals(event.extepectedEndTime) : event.extepectedEndTime != null)
            return false;
        if (ownerId != null ? !ownerId.equals(event.ownerId) : event.ownerId != null) return false;
        if (routeId != null ? !routeId.equals(event.routeId) : event.routeId != null) return false;
        if (scheduledTime != null ? !scheduledTime.equals(event.scheduledTime) : event.scheduledTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = actualEndTime != null ? actualEndTime.hashCode() : 0;
        result = 31 * result + (actualStartingTime != null ? actualStartingTime.hashCode() : 0);
        result = 31 * result + (eventDescription != null ? eventDescription.hashCode() : 0);
        result = 31 * result + (statusId != null ? statusId.hashCode() : 0);
        result = 31 * result + (extepectedEndTime != null ? extepectedEndTime.hashCode() : 0);
        result = 31 * result + (ownerId != null ? ownerId.hashCode() : 0);
        result = 31 * result + (routeId != null ? routeId.hashCode() : 0);
        result = 31 * result + (scheduledTime != null ? scheduledTime.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
