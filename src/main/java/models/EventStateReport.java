package models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "event_state_report", schema = "cycling_events_db", catalog = "")
public class EventStateReport {
    private int id;
    private Integer eventId;
    private Integer eventStateId;
    private Timestamp reportedTime;
    private Timestamp startTime;
    private Timestamp endTime;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "event_id", nullable = true)
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    @Basic
    @Column(name = "event_state_id", nullable = true)
    public Integer getEventStateId() {
        return eventStateId;
    }

    public void setEventStateId(Integer eventStateId) {
        this.eventStateId = eventStateId;
    }

    @Basic
    @Column(name = "reported_time", nullable = true)
    public Timestamp getReportedTime() {
        return reportedTime;
    }

    public void setReportedTime(Timestamp reportedTime) {
        this.reportedTime = reportedTime;
    }

    @Basic
    @Column(name = "start_time", nullable = true)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true)
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventStateReport that = (EventStateReport) o;

        if (id != that.id) return false;
        if (eventId != null ? !eventId.equals(that.eventId) : that.eventId != null) return false;
        if (eventStateId != null ? !eventStateId.equals(that.eventStateId) : that.eventStateId != null) return false;
        if (reportedTime != null ? !reportedTime.equals(that.reportedTime) : that.reportedTime != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (eventId != null ? eventId.hashCode() : 0);
        result = 31 * result + (eventStateId != null ? eventStateId.hashCode() : 0);
        result = 31 * result + (reportedTime != null ? reportedTime.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }
}
