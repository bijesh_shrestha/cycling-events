package com.app;

import data.access.MyEntityManager;
import data.access.RegisterDAO;
import models.User;
import org.json.simple.JSONObject;
import util.MD5;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

@WebServlet(name = "register")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();



        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        User user = new User();
        user.setId(new Random().nextInt(500000) + 1);
        user.setFirstName(request.getParameter("firstname"));
        user.setLastName(request.getParameter("lastname"));
        user.setEmailAddress(request.getParameter("email"));
        user.setPhoneNumber(request.getParameter("phone"));
        user.setStreetAddress(request.getParameter("street"));
        user.setCity(request.getParameter("city"));
        user.setState(request.getParameter("state"));
        user.setZipCode(request.getParameter("zip"));
        user.setUserName(request.getParameter("username"));
        user.setPassword(MD5.toMD5(request.getParameter("password")));

        JSONObject obj = RegisterDAO.registerUser(user);
        if ((boolean)obj.get("success")){
            out.print(obj);
        } else {
            out.print(obj);
        }
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
