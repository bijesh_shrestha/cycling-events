package com.app;

import data.access.LoginDAO;
import models.User;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "login")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        User user = LoginDAO.checkLogin(request.getParameter("username"), request.getParameter("password"));
        if (user != null){
            out.print("{\"success\": true}");
            // if login success, store username in session
            HttpSession session = request.getSession(true);
            session.setAttribute("user", user);
        } else {
            out.print("{\"success\": false}");
        }
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*PrintWriter out = response.getWriter();

        out.print(LoginDAO.checkLogin(request.getParameter("username"), request.getParameter("password"))+ " rows");
        out.flush();*/
        User user = (User) request.getSession(true).getAttribute("user");

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", user.getId());
        jsonObject.put("firstName", user.getFirstName());
        jsonObject.put("lastName", user.getLastName());

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        out.print(jsonObject);
        out.flush();

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        session.removeAttribute("user");
        PrintWriter out = resp.getWriter();
        out.flush();
    }
}
