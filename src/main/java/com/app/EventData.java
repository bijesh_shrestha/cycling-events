package com.app;

import data.access.MyEntityManager;
import models.User;
import models.UserEvent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.persistence.EntityManager;
import java.util.Iterator;
import java.util.List;

public class EventData {

    public static JSONArray getParticipants(int id) {
        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

        String query = "SELECT u FROM User u inner join UserEvent ue on ue.userId = u.id where ue.eventId = :eventId";

        List<User> users = entityManager.createQuery(query)
                .setParameter("eventId", id)
                .getResultList();


        JSONArray jsonArray = new JSONArray();

        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {

            User next_user = iterator.next();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("id", next_user.getId());
            jsonObject.put("firstName", next_user.getFirstName());
            jsonObject.put("lastName", next_user.getLastName());

            JSONObject subscription = new JSONObject();

            String query2 = "SELECT ue FROM UserEvent ue where ue.eventId = :eventId and ue.userId = :userId";

            UserEvent userEvent = (UserEvent) entityManager.createQuery(query2)
                    .setParameter("eventId", id)
                    .setParameter("userId", next_user.getId())
                    .getSingleResult();
            subscription.put("subscriptionTime", userEvent.getEventSubscriptionTime().toString());
            subscription.put("joiningTime", userEvent.getEventJoiningTime() == null ? "" : userEvent.getEventJoiningTime().toString());
            subscription.put("currentLocation", userEvent.getCurrentLocation());
            subscription.put("currentStatus", userEvent.getCurrentStatus() == null ? "" : userEvent.getCurrentStatus().toString());


            jsonObject.put("subscription", subscription);

            jsonArray.add(jsonObject);
        }

        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();


        return jsonArray;
    }
    public static JSONObject getOwner(int id) {
        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

        String query = "SELECT u FROM User u where id = :id";

         User user = (User)entityManager.createQuery(query)
                .setParameter("id", id)
                .getSingleResult();


            JSONObject jsonObject = new JSONObject();

            jsonObject.put("id", user.getId());
            jsonObject.put("firstName", user.getFirstName());
            jsonObject.put("lastName", user.getLastName());


        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();


        return jsonObject;
    }
}
