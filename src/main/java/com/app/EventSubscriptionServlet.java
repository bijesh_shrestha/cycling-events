package com.app;

import data.access.MyEntityManager;
import models.UserEvent;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@WebServlet("/eventSubscription")
public class EventSubscriptionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

//        String query = "Select event from Event event where id = :d";
//        entityManager.createQuery(query)
//                .setParameter(":id", Integer.valueOf(request.getParameter("eventId")));

//        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
//        java.util.Date subscriptionDate = null;
//
//
//        try {
//            subscriptionDate = format.parse(java.time.LocalDateTime.now().toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        java.sql.Date sDate = new java.sql.Date(subscriptionDate.getTime());

        UserEvent userEvent = new UserEvent();
        userEvent.setUserId(Integer.valueOf(request.getParameter("userId")));
        userEvent.setEventId(Integer.valueOf(request.getParameter("eventId")));
        userEvent.setEventSubscriptionTime(new Timestamp(new java.util.Date().getTime()));


        entityManager.persist(userEvent);

        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();

        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
